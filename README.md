# Skepties Pilot

pages:![pages build status](https://gitlab.com/skepties/pilot/badges/pages/build.svg)

## 사이트 : http://skepties.gitlab.io
## 새 글 쓰기
  * [새 글을 위한 브랜치 만들기](https://gitlab.com/skepties/pilot/branches/new)
    * 브랜치 이름은 `draft-영문제목-기타등등(저자라던가)` 예를 들면 `draft-Crank-Magnet-paul`
  * [포스트 디렉토리](https://gitlab.com/skepties/pilot/tree/master/source/_posts/articles)
    * 포스트 디렉토리 왼쪽 상단에서 브랜치를 바꿀 수 있습니다.
    * 파일이름은 `날짜-영문타이틀/주소.md` 예를 들면 `2016-10-28-Crank-Magnet.md`
  * 이미지 추가
    * [imgur](https://imgur.com) 에 올리거나
    * [이 곳에 직접 올립니다.](https://gitlab.com/skepties/pilot/tree/master/source/assets/img/2016-11)
      * 특히 메인 이미지(img:)는 이 곳에 직접 올리는 것이 좋습니다.
      * 올릴 때 브랜치를 확인하세요.

## 글쓰기 템플릿
* Front Matter: `---` 사이에 들어가야 합니다.
* 필수 Front Matter

```yaml
---
title: ""
categories:
  -
tags:
  -
---
```

* 선택 FrontMatter

```yaml
date:
author:
img: /assets/img/2016-11/
license:
o_author:
o_source:
reference:
  -
  -
```

##  [진행 보드](https://gitlab.com/skepties/pilot/boards)
