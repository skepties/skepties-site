#!/usr/bin/env python

"""A simple example of how to access the Google Analytics API."""

import json
import re
import argparse

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools



def get_service(api_name, api_version, scope, key_file_location,
                service_account_email):
  credentials = ServiceAccountCredentials.from_p12_keyfile(
    service_account_email, key_file_location, scopes=scope)

  http = credentials.authorize(httplib2.Http())

  # Build the service object.
  service = build(api_name, api_version, http=http)

  return service

def get_populars(service):
  # Use the Analytics Service Object to query the Core Reporting API
  # for the number of sessions within the past seven days.
  return service.data().ga().get(
      #ids='ga:' + profile_id,
      ids='ga:114052755',
      start_date='2016-01-01',
      end_date='today',
      metrics='ga:pageviews',
      dimensions='ga:pagePath',
      sort='-ga:pageviews',
      filters='ga:hostname==skepties.net;ga:pagePathLevel1==/p/'
      ).execute()

def LoadPostsListJSON():
  with open('_site/api/v1/posts/list-excerpt.json') as json_data:
        return json.load(json_data)

def jsonDump(data):
  #print json.dumps( data, sort_keys=True, indent=4, separators=(',', ': '),ensure_ascii=False )
  print json.dumps( data, sort_keys=True, indent=4, separators=(',', ': ') )

def main():
  # Define the auth scopes to request.
  scope = ['https://www.googleapis.com/auth/analytics.readonly']

  # Use the developer console and replace the values with your
  # service account email and relative location of your key file.
  service_account_email = 'skeptiesanalyticreader2@skepties-46571.iam.gserviceaccount.com'
  key_file_location = 'skeptiesanalyticreader2.p12'

  # Authenticate and construct service.
  service = get_service('analytics', 'v3', scope, key_file_location,
    service_account_email)
  p = re.compile('^/p/.*[/?]')
  rows = get_populars(service)['rows']
  populars = [ x for x in rows if not p.match(x[0]) ]
  for x in populars:
      if not x[0].endswith('/'):
          x[0] += '/'
  outform = { populars[i][0]:populars[i][1] for i in range(0,len(populars)) }
  f = open("skepties-site/source/_data/popular_posts.json","w")
  f.write( json.dumps( outform ))




  #print json.dumps( populars, sort_keys=True, indent=4, separators=(',', ': '),ensure_ascii=False)



if __name__ == '__main__':
  main()
