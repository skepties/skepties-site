#!/bin/bash
cd skepties-site/source;

PUB_DIR="."

_convert() {
  #while read x;do :;done
  #read x;
  S=$1 
  D=$PUB_DIR/assets/thumbs/$S; 
  #echo -e $S"\t"$D"\t"$2
  $2 | perl -ne'/(jpg|gif|png|jpeg)$/i and print' |  while read x
do 
  if [ ! -f "$D/$x" ];then
    mkdir -p $D/$(dirname $x)
    if [[ $S == "banner" ]];then 
      #convert "$x" -crop $(convert $x -format 'print"%[w]x".int(%[w]*300/1170)' info: | perl)+0+0 "$D/$x"
      convert "$x" -crop "1024x300>+0+0" "$D/$x"
    else
      convert "$x" -resize "$S"^ -crop "$S"+0+0 +repage "$D"/"$x"
    fi
  fi
done
}


echo
echo "### Generating thumbs..."
echo 
for S in "250x125" "120x53" "banner"
do
  _convert $S "find assets/img/"
  _convert $S "find img/"
  _convert $S "ls -d assets/site/*"
done

