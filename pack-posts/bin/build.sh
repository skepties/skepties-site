#!/usr/bin/env bash

SK_SRC=${SK_SRC:-skepties-site/source}     # source
SK_DES=${SK_DES:-public}                   # destination
SK_REF=${CI_BUILD_REF_NAME:-master}        # branch name

main(){
  com=${1:-build}

  case $com in
    test)
      SK_DES="_site/$SK_REF"
      time run draft_build
      time run draft_publish
      ;;
    draft_build)
      SK_DES="_site/$SK_REF"
      run draft_build
      ;;
    draft_ibuild)
      SK_DES="_site/$SK_REF"
      run draft_ibuild
      ;;
    draft)
      SK_DES="_site/$SK_REF"
      #declare -f draft_build
      run draft_build
      run draft_publish
      ;;
    publish)
      run prod_build
      ;;
  esac
}

draft_build(){
  gen_thumbs \
    && JEKYLL_ENV=development \
    bundle exec jekyll build \
    --config="$SK_SRC/_config.yml,_config_draft.yml" \
    -b "/$SK_REF" --future -D \
    -s $SK_SRC -d $SK_DES
    #-b "/$SK_REF" --future -D -V -t \
}
draft_ibuild(){
  gen_thumbs \
    && JEKYLL_ENV=development \
    bundle exec jekyll build \
    --config="$SK_SRC/_config.yml,_config_draft.yml" \
    -b "/$SK_REF" --future -D -i \
    -s $SK_SRC -d $SK_DES
}

draft_publish(){
  _gen_server_key \
    && rsync -av --delete --delete-excluded \
    --exclude=".git*" \
    -e "ssh -i .key -o'StrictHostKeyChecking=no' -p $SK_DRAFT_SERVER_PORT"  $SK_DES ${SK_DRAFT_SERVER}:${SK_DRAFT_DIR}/ \
    && ssh -i .key -p $SK_DRAFT_SERVER_PORT ${SK_DRAFT_SERVER} rebuild $(git ls-remote -h origin | cut -d/ -f3) \
    && _clean_server_key;
}

prod_build(){
  gen_thumbs \
    && JEKYLL_ENV=production \
    bundle exec jekyll build \
    --config="$SK_SRC/_config.yml,_config.yml" \
    -s $SK_SRC -d $SK_DES
}

gen_thumbs(){
  ./bin/gen_thumbs.sh
}

############## UTILS #####################
run(){
  declare -f $1
  $*
}
_gen_server_key(){
  if [ -n "$SK_DRAFT_SERVER_KEY" ];then
    echo "$SK_DRAFT_SERVER_KEY" > .key 
    chmod 400 .key
  fi
}
_clean_server_key(){
  [ -e .key ] && rm -f .key
}

main $*
